﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Shipment: IShipment
    {
        public int Compute(int poids, int hauteur)
        {
            return (int)(poids * hauteur * 0.001);
        }
    }
}
