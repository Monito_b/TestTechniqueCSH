﻿using Fclp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{

    class Program
    {
        static void Main(string[] args)
        {
            var p = new FluentCommandLineParser<ApplicationArgs>();
            string helptext = "Usage: ConsoleApp1 -l (delivery | colissimon) \nConsoleApp1 -a(--add)  \"Table Simon 150 43 150\"";
            p.Setup(arg => arg.FurnitureValue)
                .As('a', "add");

            p.Setup(arg => arg.ListFurniture)
                .As('l', "list");

            p.SetupHelp("?", "help")
                 .Callback(text => Console.WriteLine(helptext));

            var result = p.Parse(args);

            if (result.EmptyArgs == true) {
                p.HelpOption.ShowHelp(p.Options);
                System.Environment.Exit(1);
            }
            var mobiliers = new Furnitures();
            mobiliers.Deserialize(mobiliers);

            if (result.HasErrors == false && p.Object.FurnitureValue != null)
            {
                mobiliers.ParseParam(p.Object.FurnitureValue.ToString());
                System.Environment.Exit(1);
               
            } else
            {
                if (p.Object.ListFurniture == "delivery")
                {
                    mobiliers.PrintFurnituresWithLivraison();

                }
                else if (p.Object.ListFurniture == "colissimo")
                {
                    mobiliers.PrintFurnituresWithLivraisonColissimo();
                }
                else
                {
                    mobiliers.PrintFurniture();
                }
                Console.ReadLine();
                return;
            }
                
           
        }
    }
}
